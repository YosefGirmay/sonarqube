package com.cubic.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "customer")
@Entity
@Table(name = "ORDERS")
public class OrderEntity {

	@Id
	@Column(name = "ID")
	@SequenceGenerator(name = "oPk", sequenceName = "JPA_SEQ", allocationSize = 1)
	@GeneratedValue(generator = "oPk", strategy = GenerationType.SEQUENCE)
	private Long pk;
	@Column(name = "ORDER_NUM")
	private String orderNumber;

	@ManyToOne
	@JoinColumn(name = "CUSTOMER_FK")
	private CustomerEntity customer;

}
