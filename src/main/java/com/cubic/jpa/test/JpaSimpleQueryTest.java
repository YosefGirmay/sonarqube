package com.cubic.jpa.test;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import com.cubic.jpa.CustomerEntity;

public class JpaSimpleQueryTest {

	public static void main(String[] args) {
		EntityManagerFactory emf = null;
		EntityManager em = null;
		try {
			emf = Persistence.createEntityManagerFactory("CubicUnit");
			em = emf.createEntityManager();
			System.out.println("Connection Established");
			final TypedQuery<CustomerEntity> query = em.createQuery(
					"select c from CustomerEntity c", CustomerEntity.class);
			final List<CustomerEntity> entities = query.getResultList();
			entities.forEach(e -> {
				System.out.println(e);
			});

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (em != null)
				em.close();
			if (emf != null)
				emf.close();
		}
	}

}
