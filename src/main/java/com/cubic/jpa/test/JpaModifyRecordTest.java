package com.cubic.jpa.test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.cubic.jpa.CustomerEntity;

public class JpaModifyRecordTest {

	public static void main(String[] args) {
		EntityManagerFactory emf = null;
		EntityManager em = null;
		EntityTransaction et = null;
		try {
			emf = Persistence.createEntityManagerFactory("CubicUnit");
			em = emf.createEntityManager();
			System.out.println("Connection Established");
			et = em.getTransaction();
			et.begin();

			final CustomerEntity entity = em.find(CustomerEntity.class, new Long(4));
			entity.setFirstName("Mercy");
			em.persist(entity);

			et.commit();

			System.out.println("Customer Entity =" + entity);

		} catch (Exception e) {
			e.printStackTrace();
			et.setRollbackOnly();

		} finally {
			if (em != null)
				em.close();
			if (emf != null)
				emf.close();
		}
	}

}
