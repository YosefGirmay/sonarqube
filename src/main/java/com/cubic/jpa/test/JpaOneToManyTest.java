package com.cubic.jpa.test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.cubic.jpa.CustomerEntity;
import com.cubic.jpa.OrderEntity;

public class JpaOneToManyTest {

	public static void main(String[] args) {
		EntityManagerFactory emf = null;
		EntityManager em = null;
		EntityTransaction et = null;
		try {
			emf = Persistence.createEntityManagerFactory("CubicUnit");
			em = emf.createEntityManager();
			System.out.println("Connection Established");
			et = em.getTransaction();
			et.begin();

			final CustomerEntity customer = CustomerEntity.builder()
					.firstName("Dan").lastName("Turley").build();

			final OrderEntity order1 = OrderEntity.builder().customer(customer)
					.orderNumber("ORD-001").build();
			final OrderEntity order2 = OrderEntity.builder().customer(customer)
					.orderNumber("ORD-002").build();

			customer.getOrders().add(order1);
			customer.getOrders().add(order2);

			em.persist(customer);

			et.commit();

		} catch (Exception e) {
			e.printStackTrace();
			et.setRollbackOnly();

		} finally {
			if (em != null)
				em.close();
			if (emf != null)
				emf.close();
		}
	}

}
