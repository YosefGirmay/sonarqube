package com.cubic.jpa.test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.cubic.jpa.QueryConstants;

public class JpaNamedQueryTest3 {

	public static void main(String[] args) {
		EntityManagerFactory emf = null;
		EntityManager em = null;
		try {
			emf = Persistence.createEntityManagerFactory("CubicUnit");
			em = emf.createEntityManager();
			System.out.println("Connection Established");
			final Long count = (Long) em
					.createNamedQuery(QueryConstants.Q_CUSTOMER_COUNT)
					.getSingleResult();
			System.out.println("Count of records =" + count);

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (em != null)
				em.close();
			if (emf != null)
				emf.close();
		}
	}

}
