package com.cubic.jpa.test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.cubic.jpa.CustomerDetailEntity;
import com.cubic.jpa.CustomerEntity;
import com.cubic.jpa.OrderEntity;

public class JpaOneToOneTest {

	public static void main(String[] args) {
		EntityManagerFactory emf = null;
		EntityManager em = null;
		EntityTransaction et = null;
		try {
			emf = Persistence.createEntityManagerFactory("CubicUnit");
			em = emf.createEntityManager();
			System.out.println("Connection Established");
			et = em.getTransaction();
			et.begin();

			final CustomerEntity customer = CustomerEntity.builder()
					.firstName("Tim").lastName("Cook").build();
			final CustomerDetailEntity detail = CustomerDetailEntity.builder()
					.customer(customer).summary("Testing").build();
			customer.setDetail(detail);

			em.persist(customer);

			et.commit();

		} catch (Exception e) {
			e.printStackTrace();
			et.setRollbackOnly();

		} finally {
			if (em != null)
				em.close();
			if (emf != null)
				emf.close();
		}
	}

}
