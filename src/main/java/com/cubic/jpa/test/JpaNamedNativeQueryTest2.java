package com.cubic.jpa.test;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import com.cubic.jpa.CustomerEntity;
import com.cubic.jpa.QueryConstants;

public class JpaNamedNativeQueryTest2 {

	public static void main(String[] args) {
		EntityManagerFactory emf = null;
		EntityManager em = null;
		try {
			emf = Persistence.createEntityManagerFactory("CubicUnit");
			em = emf.createEntityManager();
			System.out.println("Connection Established");
			final TypedQuery<CustomerEntity> query = em.createNamedQuery(
					QueryConstants.NQ_CUSTOMER_SEARCH, CustomerEntity.class);
			query.setParameter("fName", "m%");
			query.setParameter("lName", "sm%");
			final List<CustomerEntity> entities = query.getResultList();
			entities.forEach(e -> {
				System.out.println(e);
			});

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (em != null)
				em.close();
			if (emf != null)
				emf.close();
		}
	}

}
