package com.cubic.jpa;

import javax.persistence.PostPersist;
import javax.persistence.PrePersist;

public class LogListener {

	@PrePersist
	public void beforeCreate(final CustomerEntity entity) {
		System.out.println("Inside LogListener.beforeCreate");
		System.out.println("Before = " + entity);
	}

	@PostPersist
	public void afterCreate(final CustomerEntity entity) {
		System.out.println("Inside LogListener.afterCreate");
		System.out.println("After = " + entity);
	}

}
