package com.cubic.jpa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "CUSTOMER")

@NamedNativeQueries({
		@NamedNativeQuery(name = QueryConstants.NQ_CUSTOMER_SELECT_ALL, query = "select * from CUSTOMER", resultClass = CustomerEntity.class),
		@NamedNativeQuery(name = QueryConstants.NQ_CUSTOMER_SEARCH, query = "select * from CUSTOMER c where UPPER(c.FIRST_NAME) like UPPER(:fName) OR "
				+ "UPPER(c.LAST_NAME) like UPPER(:lName)", resultClass = CustomerEntity.class)})

@NamedQueries({
		@NamedQuery(name = QueryConstants.Q_CUSTOMER_SELECT_ALL, query = "select c from CustomerEntity c"),
		@NamedQuery(name = QueryConstants.Q_CUSTOMER_SEARCH, query = "select c from CustomerEntity c where UPPER(c.lastName) like UPPER(:lName)"),
		@NamedQuery(name = QueryConstants.Q_CUSTOMER_COUNT, query = "select count(c) from CustomerEntity c"),
		@NamedQuery(name = QueryConstants.Q_CUSTOMER_SEL_COLS, query = "select c.pk,c.firstName from CustomerEntity c")})
@EntityListeners({LogListener.class})
public class CustomerEntity {
	@Id
	@Column(name = "ID")
	@SequenceGenerator(name = "cPk", sequenceName = "JPA_SEQ", allocationSize = 1)
	@GeneratedValue(generator = "cPk", strategy = GenerationType.SEQUENCE)
	private Long pk;
	@Column(name = "FIRST_NAME")
	private String firstName;
	@Column(name = "LAST_NAME")
	private String lastName;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "customer")
	private CustomerDetailEntity detail;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "customer")
	@Builder.Default
	private List<OrderEntity> orders = new ArrayList<>();


}
