package com.cubic.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "customer")
@Entity
@Table(name = "CUSTOMER_DETAIL")
public class CustomerDetailEntity {

	@Id
	@Column(name = "ID")
	@GenericGenerator(name = "dSeq", strategy = "foreign", parameters = {
			@Parameter(name = "property", value = "customer")})
	@GeneratedValue(generator = "dSeq")
	private Long pk;

	@Column(name = "SUMMARY")
	private String summary;

	@OneToOne
	@PrimaryKeyJoinColumn
	private CustomerEntity customer;

}
